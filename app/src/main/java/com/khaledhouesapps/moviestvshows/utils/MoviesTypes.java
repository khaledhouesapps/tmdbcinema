package com.khaledhouesapps.moviestvshows.utils;

public enum MoviesTypes {
    POPULAR,
    UPCOMING,
    NOW_PLAYING,
    TOP_RATED,
    LATEST,

}
