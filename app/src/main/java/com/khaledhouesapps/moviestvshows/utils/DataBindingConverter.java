package com.khaledhouesapps.moviestvshows.utils;

import android.annotation.SuppressLint;

import androidx.databinding.InverseMethod;

import com.khaledhouesapps.moviestvshows.data.entities.Genre;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class DataBindingConverter {
    @InverseMethod("dateToString")
    public static Date stringToDate(String oldValue) {
        try {
            return new SimpleDateFormat("yyyy-mm-dd hh:mm", Locale.US).parse(oldValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String dateToString(Date oldValue) {
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm");
        return dateFormat.format(oldValue);
    }

    public static String intToString(int value) {
        return Integer.toString(value);
    }

    @InverseMethod("intToString")
    public static int stringToInt(String value) {
        try {
            return Integer.valueOf(value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    public static String doubleToString(double value) {
        return Integer.toString((int) value);
    }

    @InverseMethod("doubleToString")
    public static double stringToDouble(String value) {
        try {
            return Double.valueOf(value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    public static String formatDate(String dateString) {
        if (dateString != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            try {
                Date date = formatter.parse(dateString);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(Objects.requireNonNull(date));
                SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.US);
                return monthFormat.format(date) + " " + calendar.get(Calendar.DAY_OF_MONTH) + ", " + calendar.get(Calendar.YEAR);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return dateString;
    }

    public static String getCountryFromCode(String code) {
        if (code != null) {
            Locale loc = new Locale(code);
            return loc.getDisplayLanguage(loc);
        }
        return null;
    }

    public static String formatTimeFromMinutes(int minutes) {
        String resTime = "";
        if (minutes / 60 != 0) {
            resTime = resTime + (minutes / 60) + " hr " + resTime + (minutes % 60) + " min";
        } else {
            resTime = resTime + (minutes % 60) + " min";
        }
        return resTime;
    }

    public static String getGenresFromList(List<Genre> genres) {
        StringBuilder genresString = new StringBuilder();
        if (genres != null && !genres.isEmpty()) {
            for (Genre genre : genres) {
                genresString.append(genre.getName()).append("|");
            }
            genresString.deleteCharAt(genresString.toString().length() - 1);
        }

        return genresString.toString();
    }
}