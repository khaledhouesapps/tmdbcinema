package com.khaledhouesapps.moviestvshows.utils;

public enum DataLoadingState {
    SUCCESS,
    ERROR,
    LOADING
}
