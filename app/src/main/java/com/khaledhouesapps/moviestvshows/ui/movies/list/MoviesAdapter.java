package com.khaledhouesapps.moviestvshows.ui.movies.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.data.entities.Movie;
import com.khaledhouesapps.moviestvshows.data.managers.ConfigManager;
import com.khaledhouesapps.moviestvshows.databinding.ItemMovieBinding;
import com.khaledhouesapps.moviestvshows.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.DiffUtil.DiffResult.NO_POSITION;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ItemViewHolder> {
    private List<Movie> movies = new ArrayList<>();
    private onItemClickListener listener;

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMovieBinding rvItemMovieBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_movie,
                parent,
                false);
        return new ItemViewHolder(rvItemMovieBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        Movie item = movies.get(position);
        holder.itemMovieBinding.setItem(item);
        Utils.initRatingBar(holder.itemMovieBinding.getRoot().getContext(), holder.itemMovieBinding.rtMovieRating);

        Glide.with(holder.itemMovieBinding.getRoot())
                .load(ConfigManager.getInstance().getPosterImageBaseUrl() + movies.get(position).getPosterPath())
                .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                .apply(new RequestOptions().placeholder(R.drawable.movie_placeholder))
                .into(holder.itemMovieBinding.imgMoviePoster);

        setAnimation(holder.itemMovieBinding.layoutMovieItem);
    }

    private void setAnimation(View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
        viewToAnimate.startAnimation(animation);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setNewItems(List<Movie> newItems) {
//        final DiffUtil.DiffResult result = DiffUtil.calculateDiff(new MoviesDiffCallback(movies, newItems), false);
        movies = newItems;
//        result.dispatchUpdatesTo(MoviesAdapter.this);
        if (newItems.size() > 20) {
            notifyItemInserted(newItems.size() - 1);
        } else {
            notifyDataSetChanged();
        }

    }

    public void setClickListener(onItemClickListener itemClickListener) {
        this.listener = itemClickListener;
    }

    public interface onItemClickListener {
        void onItemClick(int position);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ItemMovieBinding itemMovieBinding;

        ItemViewHolder(@NonNull ItemMovieBinding itemMovieBinding) {
            super(itemMovieBinding.getRoot());
            this.itemMovieBinding = itemMovieBinding;

            itemMovieBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null && getAdapterPosition() != NO_POSITION) {
                        listener.onItemClick(getAdapterPosition());
                    }
                }
            });
        }


    }
}