package com.khaledhouesapps.moviestvshows.ui.movies.list;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import androidx.transition.Slide;

import com.google.android.material.tabs.TabLayout;
import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.data.entities.Movie;
import com.khaledhouesapps.moviestvshows.databinding.FragmentMoviesListBinding;
import com.khaledhouesapps.moviestvshows.ui.movies.details.MovieDetailsFragment;
import com.khaledhouesapps.moviestvshows.utils.DataLoadingState;
import com.khaledhouesapps.moviestvshows.utils.Utils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;
import java.util.Objects;

public class MoviesListFragment extends Fragment {
    private MoviesListViewModel mViewModel;

    private FragmentMoviesListBinding fragmentMoviesListBinding;

    private MoviesAdapter moviesAdapter;

    public static MoviesListFragment newInstance() {
        return new MoviesListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        fragmentMoviesListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_movies_list, container, false);
        fragmentMoviesListBinding.setClickHandler(new ClickHandler());
        fragmentMoviesListBinding.rvMovies.setLayoutManager(new LinearLayoutManager(getActivity()));
        moviesAdapter = new MoviesAdapter();
        fragmentMoviesListBinding.rvMovies.setAdapter(moviesAdapter);

        return fragmentMoviesListBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MoviesListViewModel.class);

        initSearchBar();
        initTabLayout();
        fragmentMoviesListBinding.txtSectionName.setText(Objects.requireNonNull(getActivity()).getResources().getString(R.string.movies_section_title));

        fragmentMoviesListBinding.tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                ((LinearLayoutManager) Objects.requireNonNull(fragmentMoviesListBinding.rvMovies.getLayoutManager())).scrollToPositionWithOffset(0, 0);
                mViewModel.setSelectedTabIndexMutableLiveData(tab.getPosition());
                fragmentMoviesListBinding.shimmerViewContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewModel.getMoviesListLiveData().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> movies) {
                moviesAdapter.setNewItems(movies);
                fragmentMoviesListBinding.layoutRefresh.finishRefresh(500);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragmentMoviesListBinding.shimmerViewContainer.setVisibility(View.GONE);
                    }
                }, 1000);
            }
        });

        mViewModel.getMovieAPIPageCountMutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (integer > 1)
                    mViewModel.loadMovies();
            }
        });

        fragmentMoviesListBinding.rvMovies.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = Objects.requireNonNull(recyclerView.getLayoutManager()).getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if (mViewModel.getDataLoadingStateLiveData().getValue() != DataLoadingState.LOADING) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 20 * Objects.requireNonNull(mViewModel.getMovieAPIPageCountMutableLiveData().getValue())) {
                        mViewModel.setMovieAPIPageCountMutableLiveData(mViewModel.getMovieAPIPageCountMutableLiveData().getValue() + 1);
                    }
                }
            }
        });

        moviesAdapter.setClickListener(new MoviesAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mViewModel.setSelectedMovieIndexMutableLiveData(position);
            }
        });

        mViewModel.getSelectedMovieIndexMutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (integer != -1) {
                    MovieDetailsFragment movieDetailsFragment = MovieDetailsFragment.newInstance();
                    Bundle args = new Bundle();
                    args.putInt("MovieId", Objects.requireNonNull(Objects.requireNonNull(mViewModel.getMoviesListLiveData().getValue())
                            .get(Objects.requireNonNull(mViewModel.getSelectedMovieIndexMutableLiveData().getValue())).getId()));
                    movieDetailsFragment.setArguments(args);
                    movieDetailsFragment.setEnterTransition(new Slide());
                    MoviesListFragment.this.setExitTransition(new Fade());
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_layout, movieDetailsFragment)
                            .addToBackStack(null)
                            .commit();
                    mViewModel.setSelectedMovieIndexMutableLiveData(-1);
                }
            }
        });

        fragmentMoviesListBinding.layoutRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshlayout) {
                mViewModel.refreshList();
                fragmentMoviesListBinding.shimmerViewContainer.setVisibility(View.VISIBLE);
            }
        });

        fragmentMoviesListBinding.edtSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    ((LinearLayoutManager) Objects.requireNonNull(fragmentMoviesListBinding.rvMovies.getLayoutManager())).scrollToPositionWithOffset(0, 0);
                    mViewModel.setSearchQueryText(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void initTabLayout() {
        for (String tabName : mViewModel.getTabsList()) {
            fragmentMoviesListBinding.tabLayout.addTab(fragmentMoviesListBinding.tabLayout.newTab().setText(tabName));
        }

        if (Objects.requireNonNull(mViewModel.getSelectedTabIndexMutableLiveData().getValue()) != 0) {
            Objects.requireNonNull(fragmentMoviesListBinding.tabLayout.getTabAt(mViewModel.getSelectedTabIndexMutableLiveData().getValue())).select();
        }
    }

    private void initSearchBar() {
        if (mViewModel.isSearchLayoutVisible()) {
            Utils.slideDown(fragmentMoviesListBinding.layoutSearch);
            Utils.slideUp(fragmentMoviesListBinding.layoutSearch, fragmentMoviesListBinding.searchParentView);
//            Utils.showKeyboard(fragmentMoviesListBinding.edtSearchText);
        } else {
            Utils.slideDown(fragmentMoviesListBinding.layoutSearch);
        }
    }

    public class ClickHandler {
        public void onSearchButtonClick(View view) {
            if (fragmentMoviesListBinding.layoutSearch.getVisibility() == View.INVISIBLE) {
                fragmentMoviesListBinding.layoutSearch.setVisibility(View.VISIBLE);
            }
            if (!mViewModel.isSearchLayoutVisible()) {
                mViewModel.setSearchLayoutVisible(true);
                Utils.slideUp(fragmentMoviesListBinding.layoutSearch, fragmentMoviesListBinding.searchParentView);
                fragmentMoviesListBinding.layoutRefresh.setEnableRefresh(false);
                Utils.showKeyboard(fragmentMoviesListBinding.edtSearchText);
            }
        }

        public void onCloseSearchButtonClick(View view) {
            fragmentMoviesListBinding.layoutRefresh.setEnableRefresh(true);
            mViewModel.setSearchLayoutVisible(false);
            mViewModel.setSearchQueryText("");
            mViewModel.setSelectedTabIndexMutableLiveData(fragmentMoviesListBinding.tabLayout.getSelectedTabPosition());
            Utils.slideDown(fragmentMoviesListBinding.layoutSearch);
            Utils.hideKeyboard(Objects.requireNonNull(getActivity()));
            ((LinearLayoutManager) Objects.requireNonNull(fragmentMoviesListBinding.rvMovies.getLayoutManager())).scrollToPositionWithOffset(0, 0);

        }
    }

}
