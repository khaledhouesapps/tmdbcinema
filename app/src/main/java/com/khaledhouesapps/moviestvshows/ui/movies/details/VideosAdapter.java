package com.khaledhouesapps.moviestvshows.ui.movies.details;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.data.entities.Video;
import com.khaledhouesapps.moviestvshows.databinding.ItemVideoBinding;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;

import java.util.ArrayList;
import java.util.List;

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ItemViewHolder> {
    private List<Video> videos = new ArrayList<>();

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemVideoBinding itemVideoBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_video,
                parent,
                false);
        return new ItemViewHolder(itemVideoBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, int position) {

        final Video item = videos.get(position);
        holder.itemVideoBinding.setItem(item);

        holder.itemVideoBinding.youtubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
                Log.e("KHALED", "onYouTubePlayerEnterFullScreen: ");
                holder.itemVideoBinding.youtubePlayerView.enterFullScreen();
            }

            @Override
            public void onYouTubePlayerExitFullScreen() {
                Log.e("KHALED", "onYouTubePlayerExitFullScreen: ");
                holder.itemVideoBinding.youtubePlayerView.exitFullScreen();
            }
        });
        holder.itemVideoBinding.youtubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                String videoId = item.getUrlKey();
                youTubePlayer.cueVideo(videoId, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    void setNewItems(List<Video> newItems) {
        videos = newItems;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ItemVideoBinding itemVideoBinding;

        ItemViewHolder(@NonNull ItemVideoBinding itemVideoBinding) {
            super(itemVideoBinding.getRoot());
            this.itemVideoBinding = itemVideoBinding;

        }
    }
}