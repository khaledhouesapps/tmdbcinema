package com.khaledhouesapps.moviestvshows.ui.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.databinding.ActivityMainBinding;
import com.khaledhouesapps.moviestvshows.ui.movies.list.MoviesListFragment;


public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_layout, MoviesListFragment.newInstance())
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1)
            System.exit(0);
        else {
            getSupportFragmentManager().popBackStack();
        }
    }
}