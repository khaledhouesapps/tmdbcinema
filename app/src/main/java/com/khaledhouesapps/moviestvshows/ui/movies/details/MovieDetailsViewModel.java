package com.khaledhouesapps.moviestvshows.ui.movies.details;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.data.callbacks.DataLoadingCallback;
import com.khaledhouesapps.moviestvshows.data.entities.Credit;
import com.khaledhouesapps.moviestvshows.data.entities.Movie;
import com.khaledhouesapps.moviestvshows.data.entities.Review;
import com.khaledhouesapps.moviestvshows.data.entities.Video;
import com.khaledhouesapps.moviestvshows.data.repositories.MoviesRepository;
import com.khaledhouesapps.moviestvshows.utils.DataLoadingState;

import java.util.ArrayList;
import java.util.List;

public class MovieDetailsViewModel extends AndroidViewModel {
    private MoviesRepository moviesRepository;

    private LiveData<Movie> movieLiveData;
    private LiveData<List<Video>> videosLiveData;
    private LiveData<List<Credit>> creditsLiveData;
    private LiveData<List<Review>> reviewsLiveData;
    private LiveData<List<Movie>> similarMoviesListLiveData;
    private MutableLiveData<DataLoadingState> dataLoadingStateLiveData;
    private MutableLiveData<Integer> selectedTabIndexMutableLiveData;
    private MutableLiveData<Integer> selectedMovieIndexMutableLiveData;

    private List<String> tabsList;

    public MovieDetailsViewModel(@NonNull Application application) {
        super(application);
        initTabsList(application);
        moviesRepository = MoviesRepository.getInstance();
        dataLoadingStateLiveData = new MutableLiveData<>();
        dataLoadingStateLiveData.setValue(DataLoadingState.LOADING);
        videosLiveData = new MutableLiveData<>();
        creditsLiveData = new MutableLiveData<>();
        reviewsLiveData = new MutableLiveData<>();
        similarMoviesListLiveData = new MutableLiveData<>();
        selectedTabIndexMutableLiveData = new MutableLiveData<>();
        selectedTabIndexMutableLiveData.setValue(0);
        selectedMovieIndexMutableLiveData = new MutableLiveData<>();
        selectedMovieIndexMutableLiveData.setValue(-1);

    }

    LiveData<List<Video>> getVideosLiveData() {
        return videosLiveData;
    }

    MutableLiveData<DataLoadingState> getDataLoadingStateLiveData() {
        return dataLoadingStateLiveData;
    }

    MutableLiveData<Integer> getSelectedTabIndexMutableLiveData() {
        return selectedTabIndexMutableLiveData;
    }

    void setSelectedTabIndexMutableLiveData(int index) {
        this.selectedTabIndexMutableLiveData.setValue(index);
    }

    void setMovieId(final int movieId) {
        movieLiveData = moviesRepository.getMovie(movieId, new DataLoadingCallback() {
            @Override
            public void onSuccess() {

                loadVideos(movieId);
                loadCredits(movieId);
                loadReviews(movieId);
                loadSimilarMovies(movieId);


            }

            @Override
            public void onError() {
                dataLoadingStateLiveData.setValue(DataLoadingState.ERROR);
            }
        });
    }

    LiveData<Movie> getMovieLiveData() {
        return movieLiveData;
    }

    LiveData<List<Movie>> getSimilarMoviesListLiveData() {
        return similarMoviesListLiveData;
    }

    LiveData<List<Credit>> getCreditsListLiveData() {
        return creditsLiveData;
    }

    LiveData<List<Review>> getReviewsLiveData() {
        return reviewsLiveData;
    }

    MutableLiveData<Integer> getSelectedMovieIndexMutableLiveData() {
        return selectedMovieIndexMutableLiveData;
    }

    void setSelectedMovieIndexMutableLiveData(int index) {
        this.selectedMovieIndexMutableLiveData.setValue(index);
    }

    private void loadCredits(int movieId) {
        creditsLiveData = moviesRepository.getCreditsByMovie(movieId, new DataLoadingCallback() {
            @Override
            public void onSuccess() {
                dataLoadingStateLiveData.setValue(DataLoadingState.SUCCESS);
            }

            @Override
            public void onError() {
                dataLoadingStateLiveData.setValue(DataLoadingState.ERROR);
            }
        });
    }

    private void loadReviews(int movieId) {
        reviewsLiveData = moviesRepository.getReviewsByMovie(movieId, new DataLoadingCallback() {
            @Override
            public void onSuccess() {
                dataLoadingStateLiveData.setValue(DataLoadingState.SUCCESS);
            }

            @Override
            public void onError() {
                dataLoadingStateLiveData.setValue(DataLoadingState.ERROR);
            }
        });
    }

    private void loadVideos(int movieId) {
        videosLiveData = moviesRepository.getVideosByMovie(movieId, new DataLoadingCallback() {
            @Override
            public void onSuccess() {
                dataLoadingStateLiveData.setValue(DataLoadingState.SUCCESS);
            }

            @Override
            public void onError() {
                dataLoadingStateLiveData.setValue(DataLoadingState.ERROR);
            }
        });
    }

    private void loadSimilarMovies(int movieId) {

        similarMoviesListLiveData = moviesRepository.getSimilarMovies(movieId, new DataLoadingCallback() {
            @Override
            public void onSuccess() {
                dataLoadingStateLiveData.setValue(DataLoadingState.SUCCESS);
            }

            @Override
            public void onError() {
                dataLoadingStateLiveData.setValue(DataLoadingState.ERROR);
            }
        });
    }

    private void initTabsList(final Application application) {
        tabsList = new ArrayList<String>() {{
            add(application.getResources().getString(R.string.plot));
            add(application.getResources().getString(R.string.cast));
            add(application.getResources().getString(R.string.reviews));
            add(application.getResources().getString(R.string.similar));
        }};
    }

    List<String> getTabsList() {
        return tabsList;
    }
}
