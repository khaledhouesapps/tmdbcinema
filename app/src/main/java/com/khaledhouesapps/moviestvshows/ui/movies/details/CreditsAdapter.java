package com.khaledhouesapps.moviestvshows.ui.movies.details;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.data.entities.Credit;
import com.khaledhouesapps.moviestvshows.data.managers.ConfigManager;
import com.khaledhouesapps.moviestvshows.databinding.ItemCastBinding;

import java.util.ArrayList;
import java.util.List;

public class CreditsAdapter extends RecyclerView.Adapter<CreditsAdapter.ItemViewHolder> {

    private List<Credit> credits = new ArrayList<>();

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCastBinding itemCastBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_cast,
                parent,
                false);
        return new ItemViewHolder(itemCastBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        Credit item = credits.get(position);
        holder.itemCastBinding.setActor(item);

        Glide.with(holder.itemCastBinding.getRoot())
                .load(ConfigManager.getInstance().getCastProfileImageBaseUrl() + credits.get(position).getImage())
                .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                .apply(new RequestOptions().placeholder(R.drawable.reviewer))
                .into(holder.itemCastBinding.imgActorProfile);
    }

    @Override
    public int getItemCount() {
        return credits.size();
    }

    void setNewItems(List<Credit> newItems) {
        credits = newItems;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ItemCastBinding itemCastBinding;

        ItemViewHolder(@NonNull ItemCastBinding itemCastBinding) {
            super(itemCastBinding.getRoot());
            this.itemCastBinding = itemCastBinding;

        }


    }
}