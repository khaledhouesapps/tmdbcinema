package com.khaledhouesapps.moviestvshows.ui.movies.details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.data.entities.Review;
import com.khaledhouesapps.moviestvshows.databinding.ItemReviewBinding;

import java.util.ArrayList;
import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ItemViewHolder> {

    private List<Review> reviews = new ArrayList<>();

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemReviewBinding itemReviewBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_review,
                parent,
                false);
        return new ItemViewHolder(itemReviewBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        Review item = reviews.get(position);
        holder.itemReviewBinding.setReview(item);
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    void setNewItems(List<Review> newItems) {
        reviews = newItems;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ItemReviewBinding itemReviewBinding;

        ItemViewHolder(@NonNull final ItemReviewBinding itemReviewBinding) {
            super(itemReviewBinding.getRoot());
            this.itemReviewBinding = itemReviewBinding;

            itemReviewBinding.txtReviewContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemReviewBinding.txtReviewContent.getMaxLines() == 100)
                        itemReviewBinding.txtReviewContent.setMaxLines(6);
                    else
                        itemReviewBinding.txtReviewContent.setMaxLines(100);
                }
            });

        }


    }
}