package com.khaledhouesapps.moviestvshows.ui.movies.list;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.data.callbacks.DataLoadingCallback;
import com.khaledhouesapps.moviestvshows.data.entities.Movie;
import com.khaledhouesapps.moviestvshows.data.repositories.MoviesRepository;
import com.khaledhouesapps.moviestvshows.utils.DataLoadingState;
import com.khaledhouesapps.moviestvshows.utils.MoviesTypes;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MoviesListViewModel extends AndroidViewModel {
    private MoviesRepository moviesRepository;

    private LiveData<List<Movie>> moviesListLiveData;
    private MutableLiveData<DataLoadingState> dataLoadingStateLiveData;
    private MutableLiveData<Integer> movieAPIPageCountMutableLiveData;
    private MutableLiveData<Integer> selectedTabIndexMutableLiveData;
    private MutableLiveData<Integer> selectedMovieIndexMutableLiveData;
    private String searchQueryText;

    private List<String> tabsList;

    private boolean isSearchLayoutVisible = false;

    public MoviesListViewModel(@NonNull Application application) {
        super(application);
        initTabsList(application);
        moviesRepository = MoviesRepository.getInstance();
        selectedTabIndexMutableLiveData = new MutableLiveData<>();
        selectedTabIndexMutableLiveData.setValue(0);
        movieAPIPageCountMutableLiveData = new MutableLiveData<>();
        movieAPIPageCountMutableLiveData.setValue(1);
        selectedMovieIndexMutableLiveData = new MutableLiveData<>();
        selectedMovieIndexMutableLiveData.setValue(-1);
        searchQueryText = "";
        loadMovies();
    }

    LiveData<List<Movie>> getMoviesListLiveData() {
        return moviesListLiveData;
    }

    MutableLiveData<DataLoadingState> getDataLoadingStateLiveData() {
        return dataLoadingStateLiveData;
    }

    MutableLiveData<Integer> getMovieAPIPageCountMutableLiveData() {
        return movieAPIPageCountMutableLiveData;
    }

    void setMovieAPIPageCountMutableLiveData(int pageCount) {
        if (Objects.requireNonNull(moviesRepository.getMoviesTotalPagesMutableLiveData().getValue()) != pageCount) {
            this.movieAPIPageCountMutableLiveData.setValue(pageCount);
        }
    }

    MutableLiveData<Integer> getSelectedTabIndexMutableLiveData() {
        return selectedTabIndexMutableLiveData;
    }

    void setSelectedTabIndexMutableLiveData(int index) {
        this.selectedTabIndexMutableLiveData.setValue(index);
        movieAPIPageCountMutableLiveData.setValue(1);
        loadMovies();
    }

    MutableLiveData<Integer> getSelectedMovieIndexMutableLiveData() {
        return selectedMovieIndexMutableLiveData;
    }

    void setSelectedMovieIndexMutableLiveData(int index) {
        this.selectedMovieIndexMutableLiveData.setValue(index);
    }

    void loadMovies() {
        dataLoadingStateLiveData = new MutableLiveData<>();
        dataLoadingStateLiveData.setValue(DataLoadingState.LOADING);
        if (!isSearchLayoutVisible) {
            MoviesTypes type;
            switch (Objects.requireNonNull(selectedTabIndexMutableLiveData.getValue())) {
                case 0:
                    type = MoviesTypes.POPULAR;
                    break;
                case 1:
                    type = MoviesTypes.UPCOMING;
                    break;
                case 2:
                    type = MoviesTypes.NOW_PLAYING;
                    break;
                default:
                    type = MoviesTypes.LATEST;
                    break;
            }
            moviesListLiveData = moviesRepository.getMovies(Objects.requireNonNull(movieAPIPageCountMutableLiveData.getValue()), type, new DataLoadingCallback() {
                @Override
                public void onSuccess() {
                    dataLoadingStateLiveData.setValue(DataLoadingState.SUCCESS);
                }

                @Override
                public void onError() {
                    dataLoadingStateLiveData.setValue(DataLoadingState.ERROR);
                }
            });
        } else {
            searchMovies(searchQueryText);
        }
    }

    void setSearchQueryText(String searchQueryText) {
        this.searchQueryText = searchQueryText;
        movieAPIPageCountMutableLiveData.setValue(1);
        if (!searchQueryText.isEmpty()) {
            searchMovies(searchQueryText);
        }
    }

    private void searchMovies(String queryText) {
        moviesRepository.getSearchedMovies(queryText, Objects.requireNonNull(movieAPIPageCountMutableLiveData.getValue()), new DataLoadingCallback() {
            @Override
            public void onSuccess() {
                dataLoadingStateLiveData.setValue(DataLoadingState.SUCCESS);
            }

            @Override
            public void onError() {
                dataLoadingStateLiveData.setValue(DataLoadingState.ERROR);
            }
        });
    }

    private void initTabsList(final Application application) {
        tabsList = new ArrayList<String>() {{
            add(application.getResources().getString(R.string.popular));
            add(application.getResources().getString(R.string.coming_soon));
            add(application.getResources().getString(R.string.now_showing));
        }};
    }

    List<String> getTabsList() {
        return tabsList;
    }

    boolean isSearchLayoutVisible() {
        return isSearchLayoutVisible;
    }

    void setSearchLayoutVisible(boolean searchLayoutVisible) {
        isSearchLayoutVisible = searchLayoutVisible;
    }

    void refreshList() {
        movieAPIPageCountMutableLiveData.setValue(1);
        loadMovies();
    }
}
