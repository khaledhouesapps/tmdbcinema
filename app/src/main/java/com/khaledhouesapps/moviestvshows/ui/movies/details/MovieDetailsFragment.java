package com.khaledhouesapps.moviestvshows.ui.movies.details;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.tabs.TabLayout;
import com.khaledhouesapps.moviestvshows.R;
import com.khaledhouesapps.moviestvshows.data.entities.Credit;
import com.khaledhouesapps.moviestvshows.data.entities.Movie;
import com.khaledhouesapps.moviestvshows.data.entities.Review;
import com.khaledhouesapps.moviestvshows.data.entities.Video;
import com.khaledhouesapps.moviestvshows.data.managers.ConfigManager;
import com.khaledhouesapps.moviestvshows.databinding.FragmentMovieDetailsBinding;
import com.khaledhouesapps.moviestvshows.ui.movies.list.MoviesAdapter;
import com.khaledhouesapps.moviestvshows.utils.Utils;

import java.util.List;
import java.util.Objects;

import static android.view.View.GONE;

public class MovieDetailsFragment extends Fragment {
    private FragmentMovieDetailsBinding fragmentMovieDetailsBinding;

    private MovieDetailsViewModel mViewModel;
    private VideosAdapter videosAdapter;
    private MoviesAdapter moviesAdapter;
    private CreditsAdapter creditsAdapter;
    private ReviewsAdapter reviewsAdapter;

    public static MovieDetailsFragment newInstance() {
        return new MovieDetailsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        fragmentMovieDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_details, container, false);
        Objects.requireNonNull(getActivity()).getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        fragmentMovieDetailsBinding.setClickHandler(new ClickHandler());

        fragmentMovieDetailsBinding.rvVideos.setLayoutManager(new LinearLayoutManager(getActivity()));
        videosAdapter = new VideosAdapter();
        fragmentMovieDetailsBinding.rvVideos.setAdapter(videosAdapter);

        fragmentMovieDetailsBinding.rvSimilarMovies.setLayoutManager(new LinearLayoutManager(getActivity()));
        moviesAdapter = new MoviesAdapter();
        fragmentMovieDetailsBinding.rvSimilarMovies.setAdapter(moviesAdapter);

        fragmentMovieDetailsBinding.rvCast.setLayoutManager(new GridLayoutManager(getActivity(),
                Utils.calculateNoOfColumns(getActivity()) + 1));
        creditsAdapter = new CreditsAdapter();
        fragmentMovieDetailsBinding.rvCast.setAdapter(creditsAdapter);

        fragmentMovieDetailsBinding.rvReviews.setLayoutManager(new LinearLayoutManager(getActivity()));
        reviewsAdapter = new ReviewsAdapter();
        fragmentMovieDetailsBinding.rvReviews.setAdapter(reviewsAdapter);


        return fragmentMovieDetailsBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MovieDetailsViewModel.class);

        initTabLayout();
        if (getArguments() != null) {
            mViewModel.setMovieId(getArguments().getInt("MovieId"));
            mViewModel.getMovieLiveData().observe(this, new Observer<Movie>() {
                @Override
                public void onChanged(final Movie movie) {
                    fragmentMovieDetailsBinding.setMovie(movie);
                    Utils.initRatingBar(getActivity(), fragmentMovieDetailsBinding.txtMovieRating);
                    Utils.loadImageFromURL(fragmentMovieDetailsBinding.imgMovieBackground,
                            ConfigManager.getInstance().getImageBaseUrl() + movie.getBackdrop());
                    Utils.loadRoundedImageFromURL(fragmentMovieDetailsBinding.imgMoviePoster,
                            ConfigManager.getInstance().getPosterImageBaseUrl() + movie.getPosterPath());

                    mViewModel.getVideosLiveData().observe(MovieDetailsFragment.this, new Observer<List<Video>>() {
                        @Override
                        public void onChanged(List<Video> videos) {
                            if (videos.isEmpty()) {
                                fragmentMovieDetailsBinding.imgEmptyVideos.setVisibility(View.VISIBLE);
                                fragmentMovieDetailsBinding.rvVideos.setVisibility(GONE);
                            } else {
                                if (videos.size() > 8)
                                    videos.subList(8, videos.size() - 1).clear();
                                fragmentMovieDetailsBinding.imgEmptyVideos.setVisibility(GONE);
                                fragmentMovieDetailsBinding.rvVideos.setVisibility(View.VISIBLE);
                                videosAdapter.setNewItems(videos);
                            }
                        }
                    });

                    mViewModel.getCreditsListLiveData().observe(MovieDetailsFragment.this, new Observer<List<Credit>>() {
                        @Override
                        public void onChanged(List<Credit> credits) {
                            if (credits.isEmpty()) {
                                fragmentMovieDetailsBinding.imgEmptyCast.setVisibility(View.VISIBLE);
                                fragmentMovieDetailsBinding.rvCast.setVisibility(GONE);
                            } else {
                                if (credits.size() > 18)
                                    credits.subList(18, credits.size()).clear();
                                fragmentMovieDetailsBinding.imgEmptyCast.setVisibility(GONE);
                                fragmentMovieDetailsBinding.rvCast.setVisibility(View.VISIBLE);
                                creditsAdapter.setNewItems(credits);
                            }

                        }
                    });

                    mViewModel.getReviewsLiveData().observe(MovieDetailsFragment.this, new Observer<List<Review>>() {
                        @Override
                        public void onChanged(List<Review> reviews) {
                            if (reviews.isEmpty()) {
                                fragmentMovieDetailsBinding.imgEmptyReview.setVisibility(View.VISIBLE);
                                fragmentMovieDetailsBinding.rvReviews.setVisibility(GONE);
                            } else {
                                fragmentMovieDetailsBinding.imgEmptyReview.setVisibility(GONE);
                                fragmentMovieDetailsBinding.rvReviews.setVisibility(View.VISIBLE);
                                reviewsAdapter.setNewItems(reviews);
                            }
                        }
                    });

                    mViewModel.getSimilarMoviesListLiveData().observe(MovieDetailsFragment.this, new Observer<List<Movie>>() {
                        @Override
                        public void onChanged(List<Movie> movies) {
                            if (movies.isEmpty()) {
                                fragmentMovieDetailsBinding.imgEmptySimilar.setVisibility(View.VISIBLE);
                                fragmentMovieDetailsBinding.rvSimilarMovies.setVisibility(GONE);
                            } else {
                                if (movies.size() > 10)
                                    movies.subList(10, movies.size() - 1).clear();
                                fragmentMovieDetailsBinding.imgEmptySimilar.setVisibility(GONE);
                                fragmentMovieDetailsBinding.rvSimilarMovies.setVisibility(View.VISIBLE);
                                moviesAdapter.setNewItems(movies);
                            }

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    fragmentMovieDetailsBinding.shimmerViewContainer.setVisibility(View.GONE);
                                }
                            }, 1000);
                        }
                    });
                }
            });

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fragmentMovieDetailsBinding.layoutContent.setVisibility(View.VISIBLE);
                    fragmentMovieDetailsBinding.shimmerViewContainerMovie.setVisibility(View.GONE);
                }
            }, 1000);
        }

        fragmentMovieDetailsBinding.tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewModel.setSelectedTabIndexMutableLiveData(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewModel.getSelectedTabIndexMutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                switchSelectedSection(integer);
            }
        });

        mViewModel.getSelectedMovieIndexMutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (integer != -1) {
                    MovieDetailsFragment movieDetailsFragment = MovieDetailsFragment.newInstance();
                    Bundle args = new Bundle();
                    args.putInt("MovieId", Objects.requireNonNull(Objects.requireNonNull(mViewModel.getSimilarMoviesListLiveData().getValue())
                            .get(Objects.requireNonNull(mViewModel.getSelectedMovieIndexMutableLiveData().getValue())).getId()));
                    movieDetailsFragment.setArguments(args);
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_layout, movieDetailsFragment)
                            .addToBackStack(null)
                            .commit();
                    mViewModel.setSelectedMovieIndexMutableLiveData(-1);
                }
            }
        });

        moviesAdapter.setClickListener(new MoviesAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mViewModel.setSelectedMovieIndexMutableLiveData(position);
            }
        });
    }

    private void initTabLayout() {
        for (String tabName : mViewModel.getTabsList()) {
            fragmentMovieDetailsBinding.tabLayout.addTab(fragmentMovieDetailsBinding.tabLayout.newTab().setText(tabName));
        }
    }

    private void switchSelectedSection(int sectionId) {
        Animation slideInRight = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
        switch (sectionId) {
            case 0:
                fragmentMovieDetailsBinding.layoutOverview.setVisibility(View.VISIBLE);
                fragmentMovieDetailsBinding.layoutOverview.startAnimation(slideInRight);
                fragmentMovieDetailsBinding.layoutCast.setVisibility(View.GONE);
                fragmentMovieDetailsBinding.layoutReviews.setVisibility(GONE);
                fragmentMovieDetailsBinding.layoutSimilar.setVisibility(GONE);
                break;
            case 1:
                fragmentMovieDetailsBinding.layoutCast.setVisibility(View.VISIBLE);
                fragmentMovieDetailsBinding.layoutCast.startAnimation(slideInRight);
                fragmentMovieDetailsBinding.layoutOverview.setVisibility(GONE);
                fragmentMovieDetailsBinding.layoutReviews.setVisibility(GONE);
                fragmentMovieDetailsBinding.layoutSimilar.setVisibility(GONE);
                break;
            case 2:
                fragmentMovieDetailsBinding.layoutReviews.setVisibility(View.VISIBLE);
                fragmentMovieDetailsBinding.layoutReviews.startAnimation(slideInRight);
                fragmentMovieDetailsBinding.layoutOverview.setVisibility(GONE);
                fragmentMovieDetailsBinding.layoutCast.setVisibility(View.GONE);
                fragmentMovieDetailsBinding.layoutSimilar.setVisibility(GONE);
                break;
            case 3:
                fragmentMovieDetailsBinding.layoutSimilar.setVisibility(View.VISIBLE);
                fragmentMovieDetailsBinding.layoutSimilar.startAnimation(slideInRight);
                fragmentMovieDetailsBinding.layoutOverview.setVisibility(GONE);
                fragmentMovieDetailsBinding.layoutCast.setVisibility(View.GONE);
                fragmentMovieDetailsBinding.layoutReviews.setVisibility(GONE);
                break;
            default:
                break;
        }
    }

    public class ClickHandler {

        public void onBackButtonClick(View view) {
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack();
        }
    }

}
