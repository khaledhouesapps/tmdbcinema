package com.khaledhouesapps.moviestvshows.data.remote.webservices;

import com.khaledhouesapps.moviestvshows.data.entities.MoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TvShowsWebService {
    @GET("tv/popular")
    Call<MoviesResponse> getPopularTvShows(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );
}
