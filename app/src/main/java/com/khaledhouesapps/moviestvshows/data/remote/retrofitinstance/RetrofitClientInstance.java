package com.khaledhouesapps.moviestvshows.data.remote.retrofitinstance;

import com.khaledhouesapps.moviestvshows.data.managers.ConfigManager;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static Retrofit retrofit;

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(ConfigManager.getInstance().getAppRoot())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();


        }
        return retrofit;
    }
}
