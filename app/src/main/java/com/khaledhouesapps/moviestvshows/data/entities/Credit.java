package com.khaledhouesapps.moviestvshows.data.entities;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Credit {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("character")
    @Expose
    private String character;

    @SerializedName("profile_path")
    @Expose
    private String image;

    public Credit() {
    }

    public Credit(String id, String name, String character, String image) {
        this.id = id;
        this.name = name;
        this.character = character;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @NonNull
    @Override
    public String toString() {
        return "Credit{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", character='" + character + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
