package com.khaledhouesapps.moviestvshows.data.remote.webservices;

import com.khaledhouesapps.moviestvshows.data.entities.ReviewsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ReviewsWebService {
    @GET("movie/{movie_id}/reviews")
    Call<ReviewsResponse> getMovieReviews(
            @Path("movie_id") int id,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );
}
