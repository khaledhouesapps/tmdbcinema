package com.khaledhouesapps.moviestvshows.data.managers;

public class ConfigManager {
    private static final ConfigManager ourInstance = new ConfigManager();


    private ConfigManager() {

    }

    public static ConfigManager getInstance() {
        return ourInstance;
    }

    public String getApiKey() {
        return "d55cd758f9f54f8e3b6741901424c964";
    }

    public String getAppRoot() {
        String mProtocol = "https";
        String mHost = "api.themoviedb.org";
        String mApiVersion = "3";
        return mProtocol + "://" + mHost + "/" + mApiVersion + "/";
    }

    public String getImageBaseUrl() {
        return "http://image.tmdb.org/t/p/w500";
    }

    public String getPosterImageBaseUrl() {
        return "http://image.tmdb.org/t/p/w342";
    }

    public String getCastProfileImageBaseUrl() {
        return "http://image.tmdb.org/t/p/w185";
    }
}
