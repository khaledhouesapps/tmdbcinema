package com.khaledhouesapps.moviestvshows.data.repositories;


import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.khaledhouesapps.moviestvshows.data.callbacks.DataLoadingCallback;
import com.khaledhouesapps.moviestvshows.data.entities.Credit;
import com.khaledhouesapps.moviestvshows.data.entities.CreditsResponse;
import com.khaledhouesapps.moviestvshows.data.entities.Movie;
import com.khaledhouesapps.moviestvshows.data.entities.MoviesResponse;
import com.khaledhouesapps.moviestvshows.data.entities.Review;
import com.khaledhouesapps.moviestvshows.data.entities.ReviewsResponse;
import com.khaledhouesapps.moviestvshows.data.entities.Video;
import com.khaledhouesapps.moviestvshows.data.entities.VideosResponse;
import com.khaledhouesapps.moviestvshows.data.managers.ConfigManager;
import com.khaledhouesapps.moviestvshows.data.remote.retrofitinstance.RetrofitClientInstance;
import com.khaledhouesapps.moviestvshows.data.remote.webservices.CreditsWebService;
import com.khaledhouesapps.moviestvshows.data.remote.webservices.GenresWebService;
import com.khaledhouesapps.moviestvshows.data.remote.webservices.MoviesWebService;
import com.khaledhouesapps.moviestvshows.data.remote.webservices.ReviewsWebService;
import com.khaledhouesapps.moviestvshows.data.remote.webservices.VideosWebService;
import com.khaledhouesapps.moviestvshows.utils.MoviesTypes;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesRepository {
    private static final String LANGUAGE = "en-US";

    private static MoviesRepository repository;

    private MoviesWebService moviesWebService;
    private GenresWebService genresWebService;
    private VideosWebService videosWebService;
    private CreditsWebService creditsWebService;
    private ReviewsWebService reviewsWebService;

    private MutableLiveData<List<Movie>> moviesMutableLiveData;
    private MutableLiveData<Movie> movieDetailsMutableLiveData;
    private MutableLiveData<List<Movie>> similarMoviesMutableLiveData;
    private MutableLiveData<List<Video>> movieVideosMutableLiveData;
    private MutableLiveData<List<Credit>> movieCreditsMutableLiveData;
    private MutableLiveData<List<Review>> movieReviewsMutableLiveData;
    private MutableLiveData<Integer> moviesTotalPagesMutableLiveData;

    private MoviesRepository(MoviesWebService moviesWebService, GenresWebService genresWebService, VideosWebService videosWebService
            , CreditsWebService creditsWebService, ReviewsWebService reviewsWebService) {
        this.moviesWebService = moviesWebService;
        this.genresWebService = genresWebService;
        this.videosWebService = videosWebService;
        this.creditsWebService = creditsWebService;
        this.reviewsWebService = reviewsWebService;

        moviesMutableLiveData = new MutableLiveData<>();
        movieDetailsMutableLiveData = new MutableLiveData<>();
        moviesTotalPagesMutableLiveData = new MutableLiveData<>();
        movieVideosMutableLiveData = new MutableLiveData<>();
        similarMoviesMutableLiveData = new MutableLiveData<>();
        movieCreditsMutableLiveData = new MutableLiveData<>();
        movieReviewsMutableLiveData = new MutableLiveData<>();
    }

    public static MoviesRepository getInstance() {
        if (repository == null) {
            repository = new MoviesRepository(RetrofitClientInstance.getRetrofitInstance().create(MoviesWebService.class),
                    RetrofitClientInstance.getRetrofitInstance().create(GenresWebService.class),
                    RetrofitClientInstance.getRetrofitInstance().create(VideosWebService.class),
                    RetrofitClientInstance.getRetrofitInstance().create(CreditsWebService.class),
                    RetrofitClientInstance.getRetrofitInstance().create(ReviewsWebService.class));
        }

        return repository;
    }

    public LiveData<List<Movie>> getMovies(final int page, MoviesTypes sortBy, final DataLoadingCallback callback) {
        Callback<MoviesResponse> call = new Callback<MoviesResponse>() {
            @Override
            public void onResponse(@NonNull Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.isSuccessful()) {
                    MoviesResponse moviesResponse = response.body();
                    if (moviesResponse != null && moviesResponse.getMovies() != null) {
                        callback.onSuccess();
                        moviesTotalPagesMutableLiveData.postValue(moviesResponse.getTotalPages());
                        if (page == 1) {
                            moviesMutableLiveData.postValue(moviesResponse.getMovies());
                        } else {
                            List<Movie> resultMovies = moviesMutableLiveData.getValue();
                            if (resultMovies != null) {
                                resultMovies.addAll(moviesResponse.getMovies());
                            }
                            moviesMutableLiveData.postValue(resultMovies);
                        }
                    } else {
                        callback.onError();
                    }
                } else {
                    callback.onError();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MoviesResponse> call, @NonNull Throwable t) {
                callback.onError();
            }
        };

        switch (sortBy) {
            case TOP_RATED:
                moviesWebService.getTopRatedMovies(ConfigManager.getInstance().getApiKey(), LANGUAGE, page)
                        .enqueue(call);
                break;
            case UPCOMING:
                moviesWebService.getUpcomingMovies(ConfigManager.getInstance().getApiKey(), LANGUAGE, page)
                        .enqueue(call);
                break;
            case NOW_PLAYING:
                moviesWebService.getNowPlayingMovies(ConfigManager.getInstance().getApiKey(), LANGUAGE, page)
                        .enqueue(call);
                break;
            case LATEST:
                moviesWebService.getLatestMovies(ConfigManager.getInstance().getApiKey(), LANGUAGE, page)
                        .enqueue(call);
                break;
            case POPULAR:
            default:
                moviesWebService.getPopularMovies(ConfigManager.getInstance().getApiKey(), LANGUAGE, page)
                        .enqueue(call);
                break;
        }

        return moviesMutableLiveData;
    }

    public LiveData<Movie> getMovie(int movieId, final DataLoadingCallback callback) {
        moviesWebService.getMovie(movieId, ConfigManager.getInstance().getApiKey(), LANGUAGE)
                .enqueue(new Callback<Movie>() {
                    @Override
                    public void onResponse(Call<Movie> call, Response<Movie> response) {
                        if (response.isSuccessful()) {
                            Movie movie = response.body();
                            if (movie != null) {
                                callback.onSuccess();
                                movieDetailsMutableLiveData.postValue(response.body());
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(Call<Movie> call, Throwable t) {
                        callback.onError();
                    }
                });
        return movieDetailsMutableLiveData;
    }

    public LiveData<List<Movie>> getSimilarMovies(int movieId, final DataLoadingCallback callback) {
        Callback<MoviesResponse> call = new Callback<MoviesResponse>() {
            @Override
            public void onResponse(@NonNull Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.isSuccessful()) {
                    MoviesResponse moviesResponse = response.body();
                    if (moviesResponse != null && moviesResponse.getMovies() != null) {
                        callback.onSuccess();
                        similarMoviesMutableLiveData.postValue(moviesResponse.getMovies());
                    } else {
                        callback.onError();
                    }
                } else {
                    callback.onError();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MoviesResponse> call, @NonNull Throwable t) {
                callback.onError();
            }
        };

        moviesWebService.getSimilarMovies(movieId, ConfigManager.getInstance().getApiKey(), LANGUAGE)
                .enqueue(call);

        return similarMoviesMutableLiveData;
    }

    public void getSearchedMovies(String queryText, final int page, final DataLoadingCallback callback) {
        Callback<MoviesResponse> call = new Callback<MoviesResponse>() {
            @Override
            public void onResponse(@NonNull Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.isSuccessful()) {
                    MoviesResponse moviesResponse = response.body();
                    if (moviesResponse != null && moviesResponse.getMovies() != null) {
                        callback.onSuccess();
                        moviesTotalPagesMutableLiveData.postValue(moviesResponse.getTotalPages());
                        if (page == 1) {
                            moviesMutableLiveData.postValue(moviesResponse.getMovies());
                        } else {
                            List<Movie> resultMovies = moviesMutableLiveData.getValue();
                            if (resultMovies != null) {
                                resultMovies.addAll(moviesResponse.getMovies());
                            }
                            moviesMutableLiveData.postValue(resultMovies);
                        }
                    } else {
                        callback.onError();
                    }
                } else {
                    callback.onError();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MoviesResponse> call, @NonNull Throwable t) {
                callback.onError();
            }
        };

        moviesWebService.getSearchedMovies(ConfigManager.getInstance().getApiKey(), LANGUAGE, queryText, page)
                .enqueue(call);


    }

    public LiveData<List<Video>> getVideosByMovie(int movieId, final DataLoadingCallback callback) {
        videosWebService.getMovieVideos(movieId, ConfigManager.getInstance().getApiKey(), LANGUAGE)
                .enqueue(new Callback<VideosResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<VideosResponse> call, @NonNull Response<VideosResponse> response) {
                        if (response.isSuccessful()) {
                            List<Video> videos = Objects.requireNonNull(response.body()).getVideos();
                            if (videos != null) {
                                callback.onSuccess();
                                movieVideosMutableLiveData.postValue(videos);
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<VideosResponse> call, @NonNull Throwable t) {
                        callback.onError();
                    }
                });
        return movieVideosMutableLiveData;
    }

    public LiveData<List<Credit>> getCreditsByMovie(int movieId, final DataLoadingCallback callback) {
        creditsWebService.getMovieCredits(movieId, ConfigManager.getInstance().getApiKey(), LANGUAGE)
                .enqueue(new Callback<CreditsResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<CreditsResponse> call, @NonNull Response<CreditsResponse> response) {
                        if (response.isSuccessful()) {
                            List<Credit> credits = Objects.requireNonNull(response.body()).getCredits();
                            if (credits != null) {
                                callback.onSuccess();
                                movieCreditsMutableLiveData.postValue(credits);
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CreditsResponse> call, @NonNull Throwable t) {
                        callback.onError();
                    }
                });
        return movieCreditsMutableLiveData;
    }

    public LiveData<List<Review>> getReviewsByMovie(int movieId, final DataLoadingCallback callback) {
        reviewsWebService.getMovieReviews(movieId, ConfigManager.getInstance().getApiKey(), LANGUAGE)
                .enqueue(new Callback<ReviewsResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<ReviewsResponse> call, @NonNull Response<ReviewsResponse> response) {
                        if (response.isSuccessful()) {
                            List<Review> reviews = Objects.requireNonNull(response.body()).getReviews();
                            if (reviews != null) {
                                callback.onSuccess();
                                movieReviewsMutableLiveData.postValue(reviews);
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ReviewsResponse> call, @NonNull Throwable t) {
                        callback.onError();
                    }
                });
        return movieReviewsMutableLiveData;
    }

    public MutableLiveData<Integer> getMoviesTotalPagesMutableLiveData() {
        return moviesTotalPagesMutableLiveData;
    }
}
