package com.khaledhouesapps.moviestvshows.data.remote.webservices;

import com.khaledhouesapps.moviestvshows.data.entities.GenresResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GenresWebService {
    @GET("genre/movie/list")
    Call<GenresResponse> getGenres(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );
}
