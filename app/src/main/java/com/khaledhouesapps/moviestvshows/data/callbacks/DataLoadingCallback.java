package com.khaledhouesapps.moviestvshows.data.callbacks;

public interface DataLoadingCallback {
    void onSuccess();

    void onError();
}
